import 'package:sqflite/sqflite.dart';

import 'Dog.dart';
import 'database_provider.dart';

class DogDao {
  static Future<void> insertDog(Dog dog) async {
    final db = await DatabaseProvaider.database;
    await db.insert('dogs', dog.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  static Future<List<Dog>> dogs() async {
    final db = await DatabaseProvaider.database;
    return Dog.toList(await db.query('dogs'));
  }

  static Future<void> updateDog(Dog dog) async {
    final db = await DatabaseProvaider.database;

    await db.update(
      'dog',
      dog.toMap(),
      where: 'id = ?',
      whereArgs: [dog.id],
    );
  }

  static Future<void> deleteDog(int id) async {
    final db = await DatabaseProvaider.database;
    await db.delete(
      'dog',
      where: 'id = ?',
      whereArgs: [id],
    );
  }
}
