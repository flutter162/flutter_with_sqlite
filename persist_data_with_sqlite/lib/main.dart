import 'dart:async';
import 'package:sqflite/sqflite.dart';
import 'Dog.dart';
import 'database_provider.dart';
import 'dog_dao.dart';

void main() async {
  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var dido = Dog(id: 1, name: 'Dido', age: 36);
  await DogDao.insertDog(fido);
  await DogDao.insertDog(dido);

  print(await DogDao.dogs());

  fido = Dog(id: fido.id, name: fido.name, age: fido.age + 7);

  await DogDao.updateDog(fido);
  print(await DogDao.dogs());

  await DogDao.deleteDog(0);
  print(await DogDao.dogs());
}
